use crate::{GoogleUserinfo, User};
use actix_web::web;
use mongodb::coll::options::UpdateOptions;
use mongodb::db::ThreadedDatabase;
use mongodb::{bson, doc};
use r2d2::Pool;
use r2d2_mongodb::MongodbConnectionManager;
use uuid::Uuid;

pub fn index(
    pool: web::Data<Pool<MongodbConnectionManager>>,
) -> Result<Vec<User>, mongodb::error::Error> {
    let user_list = pool
        .get()
        .expect("can not get pool")
        .collection("users")
        .find(None, None)?
        .map(|u| {
            let user = u.unwrap();
            let _id = Uuid::parse_str(user.get_str("_id").unwrap()).unwrap();
            let google_id = user.get_str("google_id").unwrap().to_owned();
            let email = user.get_str("email").unwrap();
            User {
                email: email.to_owned(),
                _id,
                google_id,
            }
        })
        .collect();

    Ok(user_list)
}

pub fn get_or_create_user(
    pool: web::Data<Pool<MongodbConnectionManager>>,
    userinfo: &GoogleUserinfo,
) -> Result<User, mongodb::error::Error> {
    let user_option = pool
        .get()
        .expect("can not get pool connection")
        .collection("users")
        .find_one(Some(doc! {"google_id": userinfo.id.clone()}), None).expect("Error when getting user by google id");
    match user_option {
        Some(user_bson) => {
            let user = User {
                _id: Uuid::parse_str(user_bson.get_str("_id").unwrap()).unwrap(),
                email: user_bson.get_str("email").unwrap().to_owned(),
                google_id: user_bson.get_str("google_id").unwrap().to_owned(),
            };
            if user.email != userinfo.email && userinfo.verified_email {
                pool
                    .get()
                    .expect("can not get pool connection")
                    .collection("users")
                    .update_one(
                        doc! {"_id": user._id.to_string()},
                        doc! {"email": userinfo.email.clone()},
                        Some(UpdateOptions {
                            upsert: Some(true),
                            write_concern: None,
                        }),
                    ).expect("Could not update user");
            }
            Ok(user)
        },
        None => {
            let user = User {
                _id: Uuid::new_v4(),
                email: userinfo.email.clone(),
                google_id: userinfo.id.clone(),
            };
            let user_result = pool
                .get()
                .expect("can not get pool connection")
                .collection("users")
                .insert_one(doc!{"_id": user._id.to_string(), "email": user.email.clone()}, None)
                .unwrap();
            if user_result.acknowledged {
                Ok(user)
            } else {
                Err(mongodb::error::Error::ResponseError("Did not get user".to_owned()))
            }
        }
    }
}
