use actix_identity::{CookieIdentityPolicy, Identity, IdentityService};
use actix_session::{CookieSession, Session};
// use actix_web::cookie::{Cookie, CookieJar};
use actix_web::middleware::Logger;
use actix_web::{http, web, App, Error, HttpResponse, HttpServer};
use env_logger;
use futures::future::Future;
// use mongodb::{bson, doc, Client, ThreadedClient};
// use mongodb::db::ThreadedDatabase;
use oauth2::basic::BasicClient;
// Alternatively, this can be oauth2::curl::http_client or a custom.
use oauth2::reqwest::http_client;
use oauth2::{
    AuthUrl, AuthorizationCode, ClientId, ClientSecret, CsrfToken, PkceCodeChallenge,
    PkceCodeVerifier, RedirectUrl, Scope, TokenResponse, TokenUrl,
};
use r2d2::Pool;
use r2d2_mongodb::{ConnectionOptions, MongodbConnectionManager};
use serde::{Deserialize, Serialize};
use std::env;
use uuid::Uuid;

mod db;

// Generate secret using: export COOKIE_SECRET_KEY=`cat /dev/urandom | head -c 32 | base64`
fn main() {
    env::set_var("RUST_LOG", "actix_web=info,actix-oauth2=trace");
    env_logger::init();

    let manager = MongodbConnectionManager::new(
        ConnectionOptions::builder()
            .with_host("localhost", 27017)
            .with_db("punchy-dev")
            .build(),
    );

    let pool = Pool::builder().max_size(10).build(manager).unwrap();

    HttpServer::new(move || {
        App::new()
            .wrap(Logger::default())
            .wrap(CookieSession::signed(&[0; 32]).secure(false))
            .wrap(IdentityService::new(
                CookieIdentityPolicy::new(&[0; 32]) // <- create cookie identity policy
                    .name("punchy")
                    .secure(false),
            ))
            .data(pool.clone())
            .route("/", web::get().to(home))
            .route("/google", web::get().to(google))
            .route("/auth/google/callback", web::get().to(google_callback))
            .route("/logout", web::get().to(logout))
            .route("/users", web::get().to_async(users)) // TODO: Remove: just testing async
    })
    .bind("0.0.0.0:8080")
    .unwrap()
    .run()
    .unwrap();
}

fn home(id: Identity, session: Session) -> Result<HttpResponse, Error> {
    let mut name = "anonymous".to_owned();
    if let Some(id) = id.identity() {
        name = id
    }

    if let Some(count) = session.get::<i32>("counter")? {
        session.set("counter", count + 1)?;
    } else {
        session.set("counter", 1)?;
    }
    Ok(HttpResponse::Ok().body(format!(
        "Count is {:?}! And you are {}",
        session.get::<i32>("counter")?.unwrap(),
        name,
    )))
}

#[derive(Deserialize, Serialize)]
pub struct User {
    _id: Uuid,
    email: String,
    google_id: String,
}

fn users(
    pool: web::Data<Pool<MongodbConnectionManager>>,
) -> impl Future<Item = HttpResponse, Error = Error> {
    web::block(move || crate::db::users::index(pool)).then(|res| match res {
        Ok(users) => {
            HttpResponse::Ok().json(users)
        },
        Err(_) => HttpResponse::new(http::StatusCode::INTERNAL_SERVER_ERROR),
    })
}

#[derive(Deserialize)]
struct GoogleCallback {
    code: String,
    state: String,
}

#[derive(Deserialize)]
pub struct GoogleUserinfo {
    id: String,
    email: String,
    verified_email: bool,
}

fn google_callback(
    id: Identity,
    pool: web::Data<Pool<MongodbConnectionManager>>,
    session: Session,
    info: web::Query<GoogleCallback>,
) -> Result<HttpResponse, Error> {
    let code = AuthorizationCode::new(info.code.to_owned());
    let state = CsrfToken::new(info.state.to_owned());

    let csrf_state = session.get::<CsrfToken>("csrf_state").unwrap().unwrap();
    let pkce_code_verifier = session
        .get::<PkceCodeVerifier>("pkce_code_verifier")
        .unwrap()
        .unwrap();

    println!("Google returned the following code:\n{}\n", code.secret());
    println!(
        "Google returned the following state:\n{} (expected `{}`)\n",
        state.secret(),
        csrf_state.secret()
    );

    let google_client_id = ClientId::new(
        env::var("GOOGLE_CLIENT_ID").expect("Missing the GOOGLE_CLIENT_ID environment variable."),
    );
    let google_client_secret = ClientSecret::new(
        env::var("GOOGLE_CLIENT_SECRET")
            .expect("Missing the GOOGLE_CLIENT_SECRET environment variable."),
    );
    let auth_url = AuthUrl::new("https://accounts.google.com/o/oauth2/v2/auth".to_string())
        .expect("Invalid authorization endpoint URL");
    let token_url = TokenUrl::new("https://www.googleapis.com/oauth2/v3/token".to_string())
        .expect("Invalid token endpoint URL");

    let client = BasicClient::new(
        google_client_id,
        Some(google_client_secret),
        auth_url,
        Some(token_url),
    )
    .set_redirect_url(
        RedirectUrl::new("http://localhost:8080/auth/google/callback".to_string())
            .expect("Invalid redirect URL"),
    );

    // Exchange the code with a token.
    let token = client
        .exchange_code(code)
        .set_pkce_verifier(pkce_code_verifier)
        .request(http_client);

    //println!("Google returned the following token:\n{:?}\n", token.unwrap().access_token());

    let profile_url = "https://www.googleapis.com/oauth2/v1/userinfo";
    println!("{}", &profile_url);
    let c = reqwest::Client::new();
    let builder = c
        .get(profile_url)
        .bearer_auth(format!("{:?}", token.unwrap().access_token().secret()));
    let mut response = builder.send().unwrap();

    //println!("Google returned the following data:\n{:?}\n", &response.text());
    let userinfo: GoogleUserinfo = response.json().unwrap();

    let res = crate::db::users::get_or_create_user(pool, &userinfo);
    match res {
        Ok(user) => {
            id.remember(user._id.to_string());
            Ok(HttpResponse::Found()
                .header(http::header::LOCATION, "/").finish().into_body())
        },
        Err(_) => Ok(HttpResponse::new(http::StatusCode::FORBIDDEN)),
    }
}

#[derive(Deserialize)]
struct TestStruct {
    name: Option<String>,
}

fn google(session: Session, info: web::Query<TestStruct>) -> Result<HttpResponse, Error> {
    let default_name = "Ukjent".to_owned();
    let name = info.name.as_ref().unwrap_or(&default_name);

    let google_client_id = ClientId::new(
        env::var("GOOGLE_CLIENT_ID").expect("Missing the GOOGLE_CLIENT_ID environment variable."),
    );
    let google_client_secret = ClientSecret::new(
        env::var("GOOGLE_CLIENT_SECRET")
            .expect("Missing the GOOGLE_CLIENT_SECRET environment variable."),
    );
    let auth_url = AuthUrl::new("https://accounts.google.com/o/oauth2/v2/auth".to_string())
        .expect("Invalid authorization endpoint URL");
    let token_url = TokenUrl::new("https://www.googleapis.com/oauth2/v3/token".to_string())
        .expect("Invalid token endpoint URL");

    // Set up the config for the Google OAuth2 process.
    let client = BasicClient::new(
        google_client_id,
        Some(google_client_secret),
        auth_url,
        Some(token_url),
    )
    // This example will be running its own server at localhost:8080.
    // See below for the server implementation.
    .set_redirect_url(
        RedirectUrl::new("http://localhost:8080/auth/google/callback".to_string())
            .expect("Invalid redirect URL"),
    );

    // Google supports Proof Key for Code Exchange (PKCE - https://oauth.net/2/pkce/).
    // Create a PKCE code verifier and SHA-256 encode it as a code challenge.
    let (pkce_code_challenge, pkce_code_verifier) = PkceCodeChallenge::new_random_sha256();

    // Generate the authorization URL to which we'll redirect the user.
    let (authorize_url, csrf_state) = client
        .authorize_url(CsrfToken::new_random)
        .add_scope(Scope::new(
            "https://www.googleapis.com/auth/userinfo.email".to_string(),
        ))
        .set_pkce_challenge(pkce_code_challenge)
        .url();

    session.set("csrf_state", csrf_state)?;
    session.set("pkce_code_verifier", pkce_code_verifier)?;
    println!(
        "Open this URL in your browser:\n{}\n",
        authorize_url.to_string()
    );
    Ok(HttpResponse::Ok().body(format!(
        "<h1>Hei {}!</h1><a href=\"{}\">Google</a>",
        name, authorize_url
    )))
}

fn logout(id: Identity) -> HttpResponse {
    id.forget();
    HttpResponse::Ok().finish()
}
